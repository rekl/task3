package ru.alfa.battle.task3.exceptions;

public class BranchNotFoundExceptions extends RuntimeException{

	public BranchNotFoundExceptions(String message) {
		super(message);
	}
}
