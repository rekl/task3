package ru.alfa.battle.task3.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.alfa.battle.task3.domain.Branch;

@Repository
public interface BranchRepository extends CrudRepository<Branch, Integer> {
}
