package ru.alfa.battle.task3.service;

import org.springframework.stereotype.Service;
import ru.alfa.battle.task3.domain.Branch;
import ru.alfa.battle.task3.repo.BranchRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static java.lang.Math.*;

@Service
public class NearService {

	BranchRepository branchRepository;

	public NearService(BranchRepository branchRepository) {
		this.branchRepository = branchRepository;
	}

	public Optional<Branch> GetNearest(BigDecimal lat, BigDecimal lon){
		return StreamSupport.stream(branchRepository.findAll().spliterator(), false)
				.peek(branch -> branch.setDistance(getLenght(branch.lat,lat, branch.lon, lon)))
				.min(Comparator.comparing(Branch::getDistance));
	}

	public int getLenght(BigDecimal lat1, BigDecimal lat2, BigDecimal lon1, BigDecimal lon2){
		return (int) Math.round(2 * 6371 * asin(sqrt(sin((toRadians(lat2.doubleValue())-toRadians(lat1.doubleValue())) / 2) * sin((toRadians(lat2.doubleValue())-toRadians(lat1.doubleValue())) / 2) + cos(toRadians(lat1.doubleValue())) * cos(toRadians(lat2.doubleValue())) * sin((toRadians(lon2.doubleValue()) - toRadians(lon1.doubleValue()))/2) * sin((toRadians(lon2.doubleValue()) - toRadians(lon1.doubleValue()))/2))) * 1000);

	}

	public BigDecimal toRad(BigDecimal deg) {
		return deg.multiply(BigDecimal.valueOf(PI)).divide(BigDecimal.valueOf(180), RoundingMode.HALF_DOWN);
	}
}


