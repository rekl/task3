package ru.alfa.battle.task3.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.alfa.battle.task3.domain.Branch;
import ru.alfa.battle.task3.exceptions.BranchNotFoundExceptions;
import ru.alfa.battle.task3.repo.BranchRepository;
import ru.alfa.battle.task3.service.NearService;

import java.math.BigDecimal;

@RestController
public class ApiBranch {

	BranchRepository branchRepository;
	NearService nearService;

	public ApiBranch(BranchRepository branchRepository, NearService nearService) {
		this.branchRepository = branchRepository;
		this.nearService = nearService;
	}

	@GetMapping(path = "/branches/{id}")
	public Branch GetBranch(@PathVariable Integer id){
		return branchRepository.findById(id).orElseThrow(() -> new BranchNotFoundExceptions("branch not found"));
	}

	@GetMapping(path = "/branches")
	public Branch GetNearest(@RequestParam String lat,
								@RequestParam String lon)
	{
		return nearService.GetNearest(new BigDecimal(lat), new BigDecimal(lon)).orElseThrow(() -> new BranchNotFoundExceptions("branch not found"));
	}

}
